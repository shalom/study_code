#include <stdio.h>

main()
{
    int ch;
    while ((ch = getchar()) != EOF){
        // 换行
        if(ch == 13)
            putchar("\n");
        // 反斜杠
        if(ch == 47)
            putchar("/");
        // 回退
        if(ch == 8)
            putchar("\b");
        // 制表符
        if(ch == 28)
            putchar("\t");
    }
}