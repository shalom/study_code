#include <stdio.h>

main(){
    float lowest,hightest,step;
    float fahr,celsius;

    lowest = 0.0;
    hightest = 300.0;
    step = 20.0;

    printf("摄氏度=>华氏度\n");

    celsius = lowest;
    while (celsius <= hightest)
    {
        fahr = celsius*5/9 - 32;
        printf("%3.0f\t%.2f\n",celsius,fahr);
        celsius = celsius + step;
    }
}