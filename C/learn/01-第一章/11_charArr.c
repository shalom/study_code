#include <stdio.h>
#define MAXLINE 1000

int getLine(char line[],int lim);
void copyCharArr(char from[],char to[]);

void main()
{
    int max,len;
    char line[MAXLINE];
    char longest[MAXLINE];

    max = 0;
    // 只要读取的行长度大于0，那么就意味着还需要继续读取
    while((len = getLine(line,MAXLINE)) > 0)
    {
        if(len > max){
            max = len;
            // copy char[]
            copyCharArr(line,longest);
        }
    }

    if(max > 0)
        printf("%s\n",longest);
}



/**
 * @description: 从输入中读取一行，并返回此行的长度
 * @param {line:用来存储读取的行；lim：line的最大容量}
 * @return {返回读取的长度}
 */
int getLine(char line[],int lim)
{
    int c,i;
    // 循环读取，直到读取到EOF或者\n，并且不能超过line的最大容量
    for(i = 0 ; i < lim-1 && ((c = getchar()) != EOF && c != '\n') ; ++i)
    {
        line[i] = c;
    }
    // 将换行符也添加，以'\0'为 char[] 的结尾，这是一个规范，同时更新长度
    if(c == '\n'){
        line[i] = c;
        ++i;
    }
    // 难道length不包含最后一个字符'\0'? maybe
    line[i] = '\0';
    return i;
}

void copyCharArr(char from[],char to[])
{
    int i = 0;
    // 这里将'\0'也复制了
    while((to[i] = from[i]) != '\0')
    {
        ++i;
    }
}