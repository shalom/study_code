#include <stdio.h>

main()
{
    float lowest,hightest,step;
    lowest = 0.0;
    hightest = 300.0;
    step = 20.0;

    float fahr,celsius;
    fahr = lowest;
    while (fahr <= hightest)
    {
        /* code */
        celsius = (5.0/9.0) * (fahr - 32.0);
        printf("fahr:%3.0f  celsius:%0.2f\n",fahr,celsius);
        fahr = fahr + step;
    }
    
}