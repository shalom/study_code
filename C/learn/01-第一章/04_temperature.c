#include <stdio.h>

#define LOWER 0
#define UPPER 300
#define STEP 20

main(){
    float fahr;
    for(fahr = UPPER;fahr >= LOWER;fahr = fahr - STEP){
        printf("%6.2f%9.2f\n",fahr,(5.0/9.0) * (fahr - 32.0));
    }
}