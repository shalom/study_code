#include <stdio.h>

main()
{
    int ch, whiteNum, otherNum;
    int degitNUm[10];
    whiteNum = otherNum = 0;
    for(int i = 0;i<10;i++)
        degitNUm[i] = 0;

    printf("开始读取……\n");
    while ((ch = getchar()) != EOF)
    {
        if (ch >= '0' && ch <= '9')
        {
            // 注意 '0' != 0
            degitNUm[ch - '0']++;
        }
        else if (ch == ' ' || ch == '\t' || ch == '\n')
        {
            whiteNum++;
        }
        else
        {
            otherNum++;
        }
    }

    printf("各个数字个数：\n");
    for(int i = 0 ; i < 10 ; i++){
        printf("%d ",degitNUm[i]);
    }
    
    printf("\nwhite num: %d \n",whiteNum);
    printf("other num: %d \n",otherNum);
}