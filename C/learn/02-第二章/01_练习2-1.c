#include <limits.h>
#include <float.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

enum months
{
    JOUR = 1,
    FEB,
    MAR,
    APR
};

int main()
{
    printf("sizeof long int => %ld\n", sizeof(long int));
    printf("float min and max => %e \t %e\n", FLT_MIN, FLT_MAX);
    printf("int min and max => %d\t%d\n", INT_MIN, INT_MAX);
    printf("long int min and max => %ld\t%ld\n", LONG_MIN, LONG_MAX);
    printf("short int min and max => %d\t%d\n", SHRT_MIN, SHRT_MAX);

    unsigned char a = (unsigned char)~0;
    printf("unsigned char = %d\n", a);

    // 0000 0000
    // 1111 1111
    // 1111 1100
    // 0011 1111    2^8
    // 0x00 2^4
    // 000  2^3


    int b = strlen("sdfsdfsdf");

    printf("%d %d %d %d %d\n",JOUR,FEB,MAR,APR,b);

    unsigned x = -1;
    printf("typedef : %s\n", "sss");
}
