module studyecho

go 1.13

require (
	github.com/labstack/echo/v4 v4.1.13 // indirect
	golang.org/x/crypto v0.0.0-20200109152110-61a87790db17 // indirect
	golang.org/x/sys v0.0.0-20200107162124-548cf772de50 // indirect
)
