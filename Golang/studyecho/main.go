package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"io"
	"net/http"
	"os"
)

func main() {

	// 创建 echo 实例
	e := echo.New()

	// 配置日志
	configLogger(e)

	// 注册静态文件路由
	e.Static("img", "img")
	e.File("/favicon.ico", "img/favicon.ico")

	e.Use()

	// 设置中间件
	setMiddleware(e)

	// 注册路由
	RegisterRoutes(e)

	// 启动服务
	e.Logger.Fatal(e.Start(":2019"))

}

func configLogger(e *echo.Echo) {

	e.Logger.SetLevel(log.INFO)

	echoLog, err := os.OpenFile("log/echo.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0664)
	if err != nil {
		panic(err)
	}

	e.Logger.SetOutput(io.MultiWriter(os.Stdout, echoLog))
}

func setMiddleware(e *echo.Echo) {
	// access log 输出到文件中
	accessLog, err := os.OpenFile("log/access.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		panic(err)
	}
	// 同时输出到终端和文件
	middleware.DefaultLoggerConfig.Output = accessLog
	e.Use(middleware.Logger())
}

// AutoLogin 如果上次记住了，则自动登录
func AutoLogin(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		cookie, err := ctx.Cookie("username")
		if err == nil && cookie.Value != "" {
			// 实际项目这里可以通过 username 读库获取用户信息
			user := &User{Username: cookie.Value}

			// 放入 context 中
			ctx.Set("user", user)
		}

		return next(ctx)
	}
}

