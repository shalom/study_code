package main

import (
	"fmt"
	"html/template"
	"net/http"
)

type user struct {
	Name   string
	Gender string
	Age    int
}

func sayHello(w http.ResponseWriter, r *http.Request) {
	tem, e := template.ParseFiles("./hello.tmpl")
	if e != nil {
		fmt.Printf("template paresfile err: %v\n", e)
		return
	}

	//str:="shalom"

	//person := user{
	//	Name:   "shalom",
	//	Gender: "man",
	//	Age:    25,
	//}

	person:= map[string]interface{}{
		"name":"",
		"gender":"man",
		"age":18,
	}

	err := tem.Execute(w, person)
	if err != nil {
		fmt.Printf("template excute err: %s\n", err)
		return
	}
}

func main() {

	http.HandleFunc("/", sayHello)

	err := http.ListenAndServe(":2020", nil)
	if err != nil {
		fmt.Printf("ListenAndServe err: %s\n", err)
		return
	}

}
