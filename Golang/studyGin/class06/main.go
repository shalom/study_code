package main

import (
	"fmt"
	"html/template"
	"net/http"
)

type user struct {
	Name   string
	Gender string
	Age    int
}

func sayHello(w http.ResponseWriter, r *http.Request) {

	t, err := template.New("hello.tmpl").ParseFiles("./hello.tmpl", "ul.tmpl")
	if err!=nil{
		fmt.Printf("template ParseFiles err: %s\n",err)
	}
	person:= map[string]interface{}{
		"name":"shalom",
		"gender":"man",
		"age":18,
	}
	err = t.Execute(w, person)
	if err!=nil{
		fmt.Printf("template Execute err: %s\n",err)
	}

}

func sayHello01(w http.ResponseWriter, r *http.Request) {
	//htmlByte, err := ioutil.ReadFile("./hello.tmpl")
	//if err != nil {
	//	fmt.Println("read html failed, err:", err)
	//	return
	//}
	// 自定义一个夸人的模板函数
	kua := func(arg string) (string, error) {
		return arg + "真帅", nil
	}
	// 采用链式操作在Parse之前调用Funcs添加自定义的kua函数 (string(htmlByte))
	tmpl, err := template.New("hello1.tmpl").Funcs(template.FuncMap{"kua": kua}).ParseFiles("./hello1.tmpl")
	if err != nil {
		fmt.Println("create template failed, err:", err)
		return
	}

	user1 := user{
		Name:   "小王子",
		Gender: "男",
		Age:    18,
	}
	fmt.Printf("-------------------\n")
	// 使用user渲染模板，并将结果写入w
	tmpl.Execute(w, user1)
}

func main() {
	http.HandleFunc("/", sayHello)
	http.HandleFunc("/func",sayHello01)

	err := http.ListenAndServe(":2020", nil)
	if err != nil {
		fmt.Printf("ListenAndServe err: %s\n", err)
		return
	}

}
