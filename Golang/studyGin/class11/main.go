package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type user struct {
	Name   string `json:"name"`
	Gender string `json:"gender"`
	Age    int `json:"age"`
}

func main() {

	engine := gin.Default()


	engine.GET("/", func(c *gin.Context) {

		q:=c.Query("query")

		msg:=gin.H{
			"name":q,
			"gender":"男",
			"age":16,
		}
		c.JSON(http.StatusOK, msg)
	})


	engine.Run(":2020")
}
