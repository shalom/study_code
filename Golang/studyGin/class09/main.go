package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"html/template"
	"net/http"
)

func main() {

	engine := gin.Default()

	//	处理静态文件
	engine.Static("/xxx","./static")

	//	自定义函数
	engine.SetFuncMap(template.FuncMap{
		"safe": func(s string) template.HTML {
			fmt.Printf("%s\n",s)
			return template.HTML(s)
		},
	})

	//	两种加载模板文件的方式
	//engine.LoadHTMLFiles("./templates/index.tmpl")
	engine.LoadHTMLFiles("./templates/index.html")
	//engine.LoadHTMLGlob("./templates/**/*")

	//str := "<a>hahahahaha!</a>"
	str1 := "white"
	str2 := "black"

	//engine.GET("/", func(c *gin.Context) {
	//	c.HTML(http.StatusOK, "index.tmpl", str)
	//})
	engine.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})

	engine.GET("/black", func(c *gin.Context) {
		c.HTML(http.StatusOK, "black/index.tmpl", str2)
	})

	engine.GET("/white", func(c *gin.Context) {
		c.HTML(http.StatusOK, "white/index.tmpl", str1)
	})

	engine.Run(":2020")
}
