package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type User struct {
	Username string `json:"name" form:"username"`
	Password string `json:"pwd" form:"password"`
}

func main() {

	engine := gin.Default()

	engine.GET("/", func(c *gin.Context) {
		var user User
		err := c.ShouldBind(&user)
		if err != nil {
			c.JSON(http.StatusBadRequest, nil)
		} else {
			c.JSON(http.StatusOK, user)
		}
	})

	engine.POST("/", func(c *gin.Context) {
		var user User
		err := c.ShouldBind(&user)

		fmt.Println(c.PostForm("username"))

		if err != nil {
			c.JSON(http.StatusBadRequest, nil)
		} else {
			c.JSON(http.StatusOK, user)
		}

	})

	engine.POST("/json", func(c *gin.Context) {
		var user User
		err := c.ShouldBind(&user)

		if err != nil {
			c.JSON(http.StatusBadRequest, nil)
		} else {
			c.JSON(http.StatusOK, user)
		}

	})

	engine.Run(":2020")

}
