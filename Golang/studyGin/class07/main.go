package main

import (
	"fmt"
	"html/template"
	"net/http"
)

type user struct {
	Name   string
	Gender string
	Age    int
}

func yes(w http.ResponseWriter, r *http.Request) {

	t, err := template.New("yes.tmpl").ParseFiles("./template/base.tmpl", "./template/yes.tmpl")
	if err!=nil{
		fmt.Printf("template ParseFiles err: %s\n",err)
	}

	err = t.ExecuteTemplate(w, "yes.tmpl","yes")
	if err!=nil{
		fmt.Printf("template Execute err: %s\n",err)
	}

}

func no(w http.ResponseWriter, r *http.Request) {

	t, err := template.New("no.tmpl").ParseFiles("./template/base.tmpl", "./template/no.tmpl")
	if err!=nil{
		fmt.Printf("template ParseFiles err: %s\n",err)
	}

	err = t.ExecuteTemplate(w, "no.tmpl","no")
	if err!=nil{
		fmt.Printf("template Execute err: %s\n",err)
	}

}

func main() {
	http.HandleFunc("/yes", yes)
	http.HandleFunc("/no",no)

	err := http.ListenAndServe(":2020", nil)
	if err != nil {
		fmt.Printf("ListenAndServe err: %s\n", err)
		return
	}

}
