package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	engine := gin.Default()

	engine.GET("/:name/:age", func(c *gin.Context) {
		name:=c.Param("name")
		age:=c.Param("age")

		c.JSON(http.StatusOK,gin.H{
			"name":name,
			"age":age,
		})

	})

	engine.Run(":2020")
}
