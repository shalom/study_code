package main

import "github.com/gin-gonic/gin"

func main() {
	engine := gin.Default()

	engine.GET("/book", func(c *gin.Context) {
		c.JSON(200,gin.H{
			"method":"Get",
		})
	})

	engine.POST("/book", func(c *gin.Context) {
		c.JSON(200,gin.H{
			"method":"POST",
		})
	})

	engine.PUT("/book", func(c *gin.Context) {
		c.JSON(200,gin.H{
			"method":"PUT",
		})
	})

	engine.DELETE("/book", func(c *gin.Context) {
		c.JSON(200,gin.H{
			"method":"DELETE",
		})
	})

	engine.Run(":1995")

}
