package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	engine := gin.Default()

	engine.LoadHTMLGlob("./html/*")

	engine.GET("/login", func(c *gin.Context) {
		c.HTML(http.StatusOK, "login.html", nil)
	})

	engine.POST("/login", func(c *gin.Context) {
		username := c.PostForm("username")
		password := c.PostForm("password")


		c.HTML(http.StatusOK,"index.html",gin.H{
			"username":username,
			"password":password,
		})
	})

	engine.Run(":2020")
}
