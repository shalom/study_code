package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {

	engine := gin.Default()

	engine.LoadHTMLFiles("./upload.html")

	engine.GET("/upload", func(c *gin.Context) {
		c.HTML(http.StatusOK, "upload.html", nil)
	})

	engine.POST("/upload", func(c *gin.Context) {
		f, err := c.FormFile("上传文件")
		if err != nil {
			c.JSON(http.StatusBadRequest, nil)
		} else {
			pwd := fmt.Sprintf("./%s", f.Filename)
			c.SaveUploadedFile(f, pwd)
			c.JSON(http.StatusOK, gin.H{
				"state": "OK",
			})
		}
	})

	engine.Run(":2020")

}
