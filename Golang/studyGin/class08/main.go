package main

import (
	"fmt"
	"html/template"
	"net/http"
)

type user struct {
	Name   string
	Gender string
	Age    int
}

func hello(w http.ResponseWriter, r *http.Request) {

	t, err := template.New("index.tmpl").Delims("{[","]}").ParseFiles("./template/index.tmpl")
	if err != nil {
		fmt.Printf("template ParseFiles err: %s\n", err)
	}

	err = t.ExecuteTemplate(w, "index.tmpl", "hello world")
	if err != nil {
		fmt.Printf("template Execute err: %s\n", err)
	}

}

func no(w http.ResponseWriter, r *http.Request) {

	//save:= func(s string)template.HTML {
	//	return template.HTML(s)
	//}

	t, err := template.New("no.tmpl").ParseFiles("./template/base.tmpl", "./template/no.tmpl")
	if err != nil {
		fmt.Printf("template ParseFiles err: %s\n", err)
	}

	err = t.ExecuteTemplate(w, "no.tmpl", "no")
	if err != nil {
		fmt.Printf("template Execute err: %s\n", err)
	}

}

func main() {
	http.HandleFunc("/", hello)
	http.HandleFunc("/no", no)

	err := http.ListenAndServe(":2020", nil)
	if err != nil {
		fmt.Printf("ListenAndServe err: %s\n", err)
		return
	}

}
