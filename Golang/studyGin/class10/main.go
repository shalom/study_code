package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type user struct {
	Name   string `json:"name"`
	Gender string `json:"gender"`
	Age    int `json:"age"`
}

func main() {

	engine := gin.Default()

	engine.GET("/json01", func(c *gin.Context) {
		msg:=gin.H{
			"name":"shalom",
			"gender":"男",
			"age":16,
		}
		c.JSON(http.StatusOK, msg)
	})

	engine.GET("/json02", func(c *gin.Context) {
		msg:=user{
			"小公主",
			"女",
			15,
		}
		c.JSON(http.StatusOK,msg)
	})

	engine.Run(":2020")
}
