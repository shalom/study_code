package main

import (
	"bytes"
	"fmt"
	"github.com/labstack/echo/v4"
	"html/template"
	"net/http"
)

func main() {
	e := echo.New()

	e.GET("/login", func(c echo.Context) error {
		t,_:=template.ParseFiles("./html/login.html")

		var w bytes.Buffer
		t.Execute(&w,nil)

		return c.HTML(http.StatusOK,w.String())
	})

	e.POST("/login", func(c echo.Context) error {
		u:=c.FormValue("username")
		p:=c.FormValue("password")

		fmt.Printf("获得的用户名: %s \n获得的密码: %s \n", u, p)
		return nil
	})

	e.Logger.Fatal(e.Start(":2020"))
}
