module form

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/labstack/echo/v4 v4.1.13 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	golang.org/x/crypto v0.0.0-20200115085410-6d4e4cb37c7d // indirect
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
	golang.org/x/sys v0.0.0-20200116001909-b77594299b42 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
