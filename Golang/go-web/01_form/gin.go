package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	engine := gin.Default()

	engine.LoadHTMLFiles("./html/login.html")

	engine.GET("/login", func(c *gin.Context) {
		c.HTML(http.StatusOK,"login.html",nil)
	})

	engine.POST("/login", func(c *gin.Context) {
		username:=c.PostForm("username")
		password:=c.PostForm("password")
		fmt.Printf("获得的用户名: %s \n获得的密码: %s \n", username, password)
	})

	engine.Run(":2020")
}
