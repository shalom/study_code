package main

import (
	"html/template"
	"net/http"
	"session/session"
)

var globalSessions *session.Manager

//然后在init函数中初始化
func init() {
	globalSessions, _ = session.NewManager("memory", "gosessionid", 3600)
}

func main() {

}

func login(w http.ResponseWriter, r *http.Request) {
	//	判断本地是否有cookie，有就获取已存在的session，没有就新建一个session
	sess := globalSessions.SessionStart(w, r)

	r.ParseForm()
	if r.Method == "GET" {
		t, _ := template.ParseFiles("login.gtpl")
		w.Header().Set("Content-Type", "text/html")
		//	将session的值(sessionId)传给前端，用来标识当前状态
		t.Execute(w, sess.Get("username"))
	} else {
		sess.Set("username", r.Form["username"])
		http.Redirect(w, r, "/", 302)
	}
}